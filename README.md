# Navdy MapDownloader

![icon](https://gitlab.com/navdy-hackers/Navdy-MapDownloader/raw/master/app/src/main/res/mipmap-xxhdpi/ic_launcher.png)

A temporary workaround for downloading diskcache-v5 (for firmware ≥ 3053) or diskcache-v4 (stock firmware) offline map for Navdy.

[**Click here to download**](https://gitlab.com/navdy-hackers/Navdy-MapDownloader/tags)

## Instruction

##### Direct Update (with Android emulator):

1.  Download and **RUN** [KOPLAYER](http://www.koplayer.com) (Windows) or [Nox App Player](https://www.bignox.com/) (macOS)
2. Connect your Navdy to computer with a Micro-USB cable, and mount it to the emulator
   - For KOPLAYER: Set the Shared Folder to your Navdy `MAPS` drive
   ![KOPLAYER Shared Folder](http://i.imgur.com/Fhs8yAs.jpg)<br/>
   (For the Google Sign In prompt, you can click Sign In and then back/home to dismiss it)<br/>
   - For Nox App Player: Use [Navdy Nox Mounter](https://gitlab.com/navdy-hackers/Navdy-Nox-Mounter)<br/>
   ![Navdy Npx Mounter](https://gitlab.com/navdy-hackers/Navdy-Nox-Mounter/raw/master/preview.gif)
3. Install the Navdy MapDownloader apk ([download link](https://gitlab.com/navdy-hackers/Navdy-MapDownloader/tags))
![Install](http://img.playfun.com.tw/zhz_edt/2016/12/07/n121000000261117.png)
4. Run the MapDownloader, your Navdy device should be detected and selected automatically, if not, go back to **Step 2.**

And below is a step by step video tutorial:<br/>
[![YouTube Tutorial](http://img.youtube.com/vi/dFJ6MybIAGY/0.jpg)](http://www.youtube.com/watch?v=dFJ6MybIAGY)<br/>
http://www.youtube.com/watch?v=dFJ6MybIAGY
<br/>
##### Manually (with any Android devices)
For those who doesn't want to use an emulator, you can install and run the apk with any Android devices, and set a download custom path.
1. Run the MapDownloader
2. Set a custom download path
3. `adb pull` the folder to your computer
4.  Replace the Navdy's `.here-maps` folder manually

<br/>
<br/>
##### Credits:
Thanks to [/u/coronafire](https://www.reddit.com/user/coronafire) for discovering the `adb pull` manual update method and direct mounting with KOPLAYER
